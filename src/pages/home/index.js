import React from 'react';

import { Container } from './styles';

function home() {
    return (
        <Container>
            <section className="home__menu">
                <div className="home__menu__filter">
                    <div className="home__menu__filter-info">
                        <p>Filtrar por:</p>
                        <span>Empresa</span>
                    </div>
                    <div className="home__menu__filter-data">
                        <p>Data</p>
                        <p>Empresa</p>
                        <p>Nome</p>
                    </div>
                </div>
                <div className="home__menu__progress" />
            </section>
            <section className="home__list">
                <ul className="home__list-tittle">
                    <li>Nome</li>
                    <li>Empresa</li>
                    <li>Status</li>
                    <li>Data</li>
                    <li>Resultado</li>
                </ul>
                <ul>
                    <li>1</li>
                    <li>2</li>
                    <li>3</li>
                    <li>4</li>
                    <li>5</li>
                </ul>
                <ul>
                    <li>1</li>
                    <li>2</li>
                    <li>3</li>
                    <li>4</li>
                    <li>5</li>
                </ul>
                <ul>
                    <li>1</li>
                    <li>2</li>
                    <li>3</li>
                    <li>4</li>
                    <li>5</li>
                </ul>
                <ul>
                    <li>1</li>
                    <li>2</li>
                    <li>3</li>
                    <li>4</li>
                    <li>5</li>
                </ul>
            </section>
        </Container>
    );
}

export default home;
