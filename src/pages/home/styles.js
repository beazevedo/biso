import styled from 'styled-components';

export const Container = styled.main`
    /* background: linear-gradient(
        to right bottom,
        rgb(0, 121, 153),
        rgb(0, 158, 131)
    ); */
    padding: 0 5%;

    .home__menu {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: stretch;
        align-content: stretch;
        width: 100%;
        margin: 10px 0 20px;
    }

    .home__menu__filter {
        background: #f3efef;
        padding: 30px;
        width: 20%;
    }

    .home__menu__filter-info {
        display: flex;
        flex-direction: row;
        position: relative;
        font-size: 20px;
        margin: 3px;

        p {
            padding-right: 10px;
        }

        span:after {
            content: '';
            display: inline-block;
            width: 8px;
            height: 8px;
            background: transparent;
            border-right: 2px solid rgb(0, 121, 153);
            border-bottom: 2px solid rgb(0, 121, 153);
            transform: rotate(45deg);
            margin-left: 20px;
            margin-bottom: 3px;
        }
    }

    .home__menu__filter-data {
        position: absolute;
        font-size: 20px;
        margin: 3px;
        padding: 30px;
        background: #f3efef;
        width: 207px;
        left: 69px;
        text-align: center;
        top: -200px;

        p {
            line-height: 30px;
        }
    }

    .home__menu__progress {
        background: #f3efef;
        width: 70%;
        display: flex;
        padding: 10px;
        align-items: center;

        ::before {
            content: '';
            width: 50%;
            height: 15px;
            background: rgb(0, 121, 153);
            border-radius: 50px;
        }
    }

    .home__list {
        width: 100%;
        background: #fff;
        padding: 10px;
    }

    .home__list-tittle {
        margin-bottom: 10px;
        padding: 5px;
    }

    .home__list ul {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
    }

    .home__list li {
        color: #333;
        font-weight: 300;
        font-size: 12px;
        text-transform: uppercase;
        padding: 5px;
    }
`;
