import styled from 'styled-components';

export const Container = styled.div`
    font-family: 'Montserrat', sans-serif;
    width: 100vw;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    background: linear-gradient(to right bottom, rgb(0, 121, 153), rgb(0, 158, 131));



    .login__box {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        width: 50%;
        min-width: 400px;
        height: 300px;
        background: #fff;
        border-radius: 10px;
        );
    }


    .login__box-tittle {
        font-size: 25px;
        color: rgba(0, 121, 153, 0.8);
        margin-bottom: 20px;
    }

    .login__box-input {
        width: 200px;
        height: 30px;
        margin-bottom: 10px;
        border-radius: 5px;
        border: 2px solid #ccc;
        padding: 5px;
    }

    .login__box-button {
        width: 215px;
        height: 40px;
        color: #fff;
        background: rgba(0, 121, 153, 0.8);
        margin-top: 20px;
        border: 2px rgba(0, 121, 153, 0.8);
        border-radius: 5px;
        font-size: 16px;
        padding: 5px;
    }

`;
