import React from 'react';

import { Container } from './styles';

function login() {
    return (
        <Container>
            <div className="login__box">
                <h1 className="login__box-tittle">Login</h1>
                <label htmlFor="username" />
                <input className="login__box-input" placeholder="Username" />
                <label htmlFor="password" />
                <input className="login__box-input" placeholder="Password" />
                <button className="login__box-button"> Entrar</button>
            </div>
        </Container>
    );
}

export default login;
